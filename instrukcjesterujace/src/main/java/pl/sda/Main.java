package pl.sda;

public class Main {
    public static void main(String[] args) {

        boolean czyWykonac = false;

        int liczba = 4;

        // wykluczająca się alternatywa
        if (liczba == 5) {
            System.out.println("Wykonuje blok if.");
        } else if (liczba >= 5) {
            System.out.println("Wykonuje blok if else.");
        } else {
            System.out.println("Jeśli warunek nie jest spełniony");
        }

        // sprawdzenie dwóch oddzielnych warunków
        if (liczba == 5) {
            System.out.println("Wykonuje blok if.");
        }
        if (liczba >= 5) {
            System.out.println("Wykonuje blok if else.");
        } else {
            System.out.println("Jeśli warunek nie jest spełniony");
        }

        System.out.println("Koniec");
    }
}
