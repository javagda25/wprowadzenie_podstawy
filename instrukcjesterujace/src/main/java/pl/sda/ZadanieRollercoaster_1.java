package pl.sda;

public class ZadanieRollercoaster_1 {
    public static void main(String[] args) {
        int waga = 150;
        int wzrost = 130;
        int wiek = 50;

        boolean warunek_waga = waga < 180;
        boolean warunek_wiek = wiek > 10 && wiek < 80;
        boolean warunek_wzrost = wzrost > 150 && wzrost < 220;

        if (warunek_waga && warunek_wiek && warunek_wzrost) {
            System.out.println("Wszystko ok! Możesz wejść!");
        } else {
            System.out.println("Nie możesz wejść!");
            if (!warunek_waga) { // warunek wagi nie jest spełniony
                System.out.println("Nie możesz wejść, jesteś zbyt ciężki/a");
            }
            if (!warunek_wiek) { // warunek wieku nie jest spełniony
                System.out.println("Nie możesz wejść z powodu wieku!");
            }
            if (!warunek_wzrost) {
                System.out.println("Nie możesz wejść z powodu wzrostu!");
            }
        }
    }
}
