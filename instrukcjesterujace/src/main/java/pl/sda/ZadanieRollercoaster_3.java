package pl.sda;

public class ZadanieRollercoaster_3 {
    public static void main(String[] args) {
        int waga = 150;
        int wzrost = 130;
        int wiek = 50;

        if (waga > 180) {
            System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt ciężki/a");
        } else if (wzrost < 150) {
            System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt niski/a");
        } else if (wzrost > 220) {
            System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt wysoki/a");
        } else if (wiek < 10) {
            System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt młody/a");
        } else if (wiek > 85) {
            System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt stary/a");
        } else {
            System.out.println("możesz wejść na kolejkę");
        }

    }
}
