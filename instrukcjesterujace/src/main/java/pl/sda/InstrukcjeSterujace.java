package pl.sda;

public class InstrukcjeSterujace {
    public static void main(String[] args) {
        if (2 > 3) {
            System.out.println("2>3 :)");
        } else {
            System.out.println("2>3 :(");
        }

        if (4 < 5) {
            System.out.println("4<5 :)");
        } else {
            System.out.println("4<5 :(");
        }

        if ((2 - 2) == 0) {
            System.out.println("(2-2)==0 :)");
        } else {
            System.out.println("(2-2)==0 :(");
        }

        if (true) {
            System.out.println("true :)");
        } else {
            System.out.println("true :(");
        }

        if (9 % 2 == 0) {
            System.out.println("9%2==0 :)");
        } else {
            System.out.println("9%2==0 :(");
        }

        if (9 % 3 == 0) {
            System.out.println("9%3==0 :)");
        } else {
            System.out.println("9%3==0 :(");
        }

    }
}
