package pl.sda;

public class SwitchDemo {
    public static void main(String[] args) {
        int liczba = 60;

        switch (liczba) {
            case 50: // tylko pod warunkiem przyrównania liczba == 50
//                break;
            case 60:
                System.out.println("mamy 60 lub 50");
//                System.out.println("Wykonaj 50");
//                break;
            case 100:
                System.out.println("Mamy setkę");
//                break;
            default:
                System.out.println("Nic nie pasuje");
//                break;
        }
    }
}
