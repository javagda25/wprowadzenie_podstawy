package pl.sda;

import java.util.Scanner;

public class MainZadanieSprawdzenieImienia {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj swoje imie:");
        String imie = scanner.nextLine();

        //if (imie == "Ania") { // < -- tak NIE ROBIMY!! BEBE !
        if (imie.equals("Ania")) {
            System.out.println("Cześć szefowa!");
        } else {
            System.out.println("Cześć " + imie + "!");
        }
    }
}
