package pl.sda;

public class PrzykladWhile {
    public static void main(String[] args) {

        for (int i = 0; i < 100; i++) {
            double cos = 5;
            System.out.println(i);
            cos ++;
            System.out.println(cos);
        }

        double cos = 6;

        // pętla for która jest u góry.
        int i = 0;
        while (i < 100) {
            System.out.println(i);

            i++;
        }



    }
}
