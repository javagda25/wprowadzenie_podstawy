package pl.sda;

import java.util.Scanner;

public class PetlaDoWhile {
    public static void main(String[] args) {

//        int i = 0;
//        while (i < 100) {
//            System.out.println(i);
//
//            i++;
//        }

        // odpowiadająca powyższej pętli while pętla do while  (poniżej):
        int i = 0;
//         blok kodu do musi się wykonać co najmniej jeden raz.
        do {
            System.out.println(i);
            i++;
        } while (i < 100);

        Scanner scanner = new Scanner(System.in);
        int liczba;
        do {
            liczba = scanner.nextInt();

            System.out.println("Wpisane: " + liczba);

        } while (liczba != 100);
    }
}
