package pl.sda;

import java.util.Scanner;

public class DoWhileZTekstem {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String tekstWpisany;
        do {
            tekstWpisany = scanner.next();

            System.out.println(tekstWpisany);
        } while (tekstWpisany.equals("quit"));
    }
}
