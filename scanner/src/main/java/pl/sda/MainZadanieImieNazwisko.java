package pl.sda;

import java.util.Scanner;

public class MainZadanieImieNazwisko {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj swoje imie:");
        String imie = scanner.next();

        System.out.println("Podaj swoje nazwisko:");
        String nazwisko = scanner.next();

        System.out.println("Witaj " + imie + " " + nazwisko + "!");
    }
}
