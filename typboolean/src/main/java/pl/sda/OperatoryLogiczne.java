package pl.sda;

public class OperatoryLogiczne {
    public static void main(String[] args) {
        System.out.println(false == false); // true
        System.out.println(false != true); // true
        System.out.println(!true);      // false
        System.out.println(2 > 4);      // false
        System.out.println(3 > 5);      // false
        System.out.println(3 == 3 && 3 == 4); // false
        System.out.println(3 != 5 || 3 == 5); // true
        System.out.println((2 + 4) > (1 + 3)); // true
        System.out.println("cos".equals("cos")); // true
        System.out.println("cos" == "cos"); // true

        String zmienna1 = new String("cos");
//        String zmienna1 = "cos";
        String zmienna2 = new String("cos");

        System.out.println();
        System.out.println(zmienna1.equals(zmienna2)); // porównanie treści zmiennych (zawartości string)
        System.out.println(zmienna1 == zmienna2); // porównanie adresów obiektów (różne adresy)

    }
}
