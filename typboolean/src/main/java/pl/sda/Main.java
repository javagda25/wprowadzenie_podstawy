package pl.sda;

public class Main {
    public static void main(String[] args) {
        int liczba = 0;

        System.out.println(liczba++); // post inkrementacja - najpierw wypisz, potem zwiększ o 1
        System.out.println(++liczba); // pre inkrementacja - zwiększ o 1, potem wypisz

        // to samo do dekrementacji (--)

        // wartość obecna liczba = 2

        liczba = liczba + 2;
        liczba += 2;

        liczba *= 2; // przemnóż wynik zmiennej liczba *2


        boolean zmiennaBoolean = true;

        System.out.println(zmiennaBoolean != true);
        // zmiennaBoolean jest różna od true
        // wypisze na ekran "false"

    }
}
