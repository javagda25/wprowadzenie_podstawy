package pl.sda;

public class RzutowanieTypow {
    public static void main(String[] args) {
        // rzutowanie to zmiana typu

        int liczba = 266;
        float innaLiczba = liczba;

        System.out.println(innaLiczba);
        liczba = (int) innaLiczba; //wymuszone rzutowanie (świadome)
        System.out.println(liczba);

        byte okrojony = (byte) liczba;
        System.out.println(okrojony);

        long duzo = 50;
        float rzutowanyLong = duzo;
        System.out.println(rzutowanyLong);


    }
}
