package pl.sda.zmienna;

public class Main {
    public static void main(String[] args) {
        int liczba; // deklaracja

        liczba = 5; // inicjalizacja / przypisanie

        int liczbaZadeklarowan = 5; // deklaracaja z inicjalizacją

        boolean flaga;  // oznacza wartość tak/nie (true/false) (1/0)
        byte znak;      // 8 bitów (wartość -127 - 128)
        char znaczek;   // 8 bitów (wartość od 0 do 255)
        short krotkaLiczba; // 16 bitów (0 - 65535) (-32767 - 32765)
        float zmiennoPrzecinkowa; // 16 bitów
        // int
        long dlugaLiczba; // 64 bity (duuuuża liczba)
        double zmiennoPrzecinkowaPodwojnejPrecyzji; // 32 bity

        String tekst;


        // Zadanie 1:
        // Stwórz Klasę: MainImie w package'u z klasą Main i w niej, w metodzie "main"
        //              stwórz zmienną (zmienną nazwij "mojeImie") ze swoim imieniem
        //              i nazwiskiem, a następnie wypisz je na ekran do konsoli.

        /*
            Obszar komentarza
         */
    }
}
